import re
import logging
import csv

log = logging.getLogger(__name__)


class NessusIngester:
    """ Parses the output of Nessus CSV files for vulnerabilities.
    """

    def __init__(self):
        self.name = 'Nessus CSV'
        self.id = 'nessus'

    def detect(self, tooloutput):
        """ Detects whether the supplied info string is a Nessus CSV file
        """

        detectre = re.compile(
            r'^Plugin ID,CVE,CVSS,Risk,Host,Protocol')
        if detectre.match(tooloutput):
            log.debug("Did not match to a Nessus CSV file")
            return True
        else:
            log.debug("Matched to a Nessus CSV file")
            return False

    def ingest(self, tooloutput):
        """ Ingests the Nessus CSV file and matches plugin IDs to 
        """
        # This is the state we'll be using
        # A line by line version of the output
        csvsplit = tooloutput.splitlines()
        nessuscsv = csv.DictReader(csvsplit)
        nessusvulns = {}

        # Go through the CSV, group all the vulns by Plugin ID so we can match to writeups
        for vuln in nessuscsv:
            # If there's no risk don't bother
            if vuln['Risk'] == "None":
                continue

            pluginid = int(vuln['Plugin ID'])

            if pluginid not in nessusvulns:
                nessusvulns[pluginid] = [vuln]
            else:
                nessusvulns[pluginid] += [vuln]

        # Sort out all the SSL vulnerabilities:
        sslvulns = []
        sslplugins = {51192: "SSL certificate cannot be trusted",
                      42873: "SWEET32",
                      83875: "Logjam",
                      65821: "Bar Mitzvah"
                      }
        for sslplugin in sslplugins:
            if sslplugin in nessusvulns:
                for vuln in nessusvulns[sslplugin]:
                    sslvulns.append(
                        vuln
                    )
        if len(sslvulns) > 0:
            nessusvulns['ssl'] = sslvulns

        return nessusvulns

    def process(self, vulns):
        """ Process each vuln ID and provide helper tables, etc, to make processing the data easier.
        """

        # Build SSL tables
        if 'ssl' in vulns:
            ssltable = []
            sslplugins = {51192: "SSL certificate cannot be trusted",
                          42873: "SWEET32",
                          83875: "Logjam",
                          65821: "Bar Mitzvah"
                          }
            for vuln in vulns['ssl']['data']:
                ssltable.append(
                    {
                        'Host': vuln['Host'],
                        'Port': vuln['Port'],
                        'Vulnerability': sslplugins[int(vuln['Plugin ID'])]
                    }
                )
            vulns['ssl']['ssltable'] = ssltable

        return vulns
