import re
import logging
from datetime import date, datetime

log = logging.getLogger(__name__)


class WPScanIngester:
    """ Parses the output of WPScan files for vulnerabilities and out-of-date plugins, themes, and software.

    Vulns that it might return:
    - plugins_outofdate: A list of out-of-date plugins in the format { url, ip, time_of_scan, plugins }. Will only show insecure plugins.
    - wp_outofdate: Wordpress version out of date. Format: { url, ip, time_of_scan, version, insecure_version_date}
    """

    def __init__(self):
        self.name = 'WPScan'
        self.id = 'wpscan'

    def detect(self, tooloutput):
        """ Detects whether the supplied tooloutput string is a WPScan output
        """

        detectre = re.compile(
            r'^([\s\\\/\_\(\)\|®\`\'\,]*\n)*\s+WordPress Security Scanner by the WPScan Team')
        if detectre.match(tooloutput):
            log.debug("Did not match to a wpscan file")
            return True
        else:
            log.debug("Matched to a wpscan file")
            return False

    def _get_plugin(self):
        re_pluginline = re.compile(r'^ \|')
        re_pluginversion = re.compile(
            r'^ \| Version: ([^ ]*) \((\d{1,3})% confidence\)')
        re_lastupdated = re.compile(
            r'^ \| Last Updated: ([0-9\-T\:]*)')
        re_latestversion = re.compile(
            r'^ \| [^ ]+ The version is out of date, the latest version is ([^ ]*)$')
        re_location = re.compile(
            r'^ \| Location: (.*)$'
        )

        plugininfo = {
            'name': '',
            'location': '',
            'version': '',
            'confidence': 0,
            'lastupdated': None,
            'outofdate': False,
            'latestversion': ''
        }

        while re_pluginline.match(self.wpso[self.nline]):
            curline = self.wpso[self.nline]
            self.nline += 1

            versionmatch = re_pluginversion.match(curline)
            if versionmatch:
                plugininfo['version'] = versionmatch.group(1)
                plugininfo['confidence'] = versionmatch.group(2)
                continue

            lumatch = re_lastupdated.match(curline)
            if lumatch:
                dts = str(lumatch.group(1))
                dt = datetime.strptime(dts,
                                       '%Y-%m-%dT%H:%M:%S')
                plugininfo['lastupdated'] = dt

            lvmatch = re_latestversion.match(curline)
            if lvmatch:
                plugininfo['latestversion'] = lvmatch.group(1)
                plugininfo['outofdate'] = True

            locmatch = re_location.match(curline)
            if locmatch:
                plugininfo['location'] = locmatch.group(1)

        return plugininfo

    def _get_input(self):
        # The line showing the wordpress version
        re_wpversion = re.compile(
            r'^.*WordPress version (.*) identified( \(Insecure, released on ([0-9\-]*)\))?')
        # We will now be having a list of plugins
        re_pluginsidentified = re.compile(r'^.*Plugin\(s\) Identified:$')
        re_pluginheader = re.compile(r'^[^ ]* ([a-z\-]*)$')
        re_noconfig = re.compile(r'^.*No Config Backups Found.$')
        re_url = re.compile(r'^[^ ]* URL: ([^ ]*) \[([0-9\.]+)\]')
        re_date = re.compile(r'^[^ ]* Started: (.*)$')

        curline = self.wpso[self.nline]

        wpvm = re_wpversion.match(curline)
        if wpvm:
            version = wpvm.group(1)
            if len(wpvm.groups()) > 2:
                lrm = str(wpvm.group(3))
                lrdt = datetime.strptime(lrm,
                                         '%Y-%m-%d').date()
                self.output['insecure'] = True
                self.output['insecure_version_date'] = lrdt

            self.output['version'] = version
            self.section = ''
            return

        urlm = re_url.match(curline)
        if urlm:
            self.output['url'] = str(urlm.group(1))
            self.output['ip'] = str(urlm.group(2))

        datem = re_date.match(curline)
        if datem:
            dts = str(datem.group(1))
            dt = datetime.strptime(dts,
                                   '%a %b %d %H:%M:%S %Y')
            self.output['time_of_scan'] = dt

        if re_pluginsidentified.match(curline):
            # self.output.append(self._get_plugin())
            self.section = 'plugins'
            return

        if re_noconfig.match(curline):
            self.section = ''
            return

        if self.section == 'plugins':
            phm = re_pluginheader.match(curline)
            if phm:
                self.nline += 1
                plugininfo = self._get_plugin()
                plugininfo['name'] = phm.group(1)
                self.output['plugins'].append(plugininfo)

    def ingest(self, tooloutput):
        """ Ingests the WPScan output. This uses a bunch of helper functions to unwrap the output.
        """
        # This is the state we'll be using
        self.wpso = tooloutput.splitlines()  # A line by line version of the output
        self.nline = 0              # What line we're up to in the output
        self.section = ''
        self.output = {'plugins': [], 'insecure': False, 'ip': '', 'url': '', 'version': '',
                       'time_of_scan': datetime.now(), 'insecure_version_date': datetime.now()}

        while self.nline < len(self.wpso):
            self._get_input()
            self.nline += 1

        # Format the vulns:
        vulns = {}

        # Out of date plugins
        plugins_outofdate = {'plugins': list(
            filter(lambda x: x['outofdate'], self.output['plugins']))}
        if len(plugins_outofdate['plugins']) > 0:
            for value in 'url', 'ip', 'time_of_scan':
                plugins_outofdate[value] = self.output[value]

            # Generate a helper plugin table
            plugins_outofdate['pluginstable'] = []
            for plugin in plugins_outofdate['plugins']:
                if plugin['lastupdated']:
                    plugins_outofdate['pluginstable'].append([
                        plugin['name'], plugin['version'], plugin['lastupdated'].strftime("%-d %B %Y")])
                else:
                    plugins_outofdate['pluginstable'].append([
                        plugin['name'], plugin['version'], ''])

            vulns['plugins_outofdate'] = plugins_outofdate

        # Wordpress out of date
        if self.output['insecure']:
            wp_outofdate = {}
            for value in 'url', 'ip', 'time_of_scan', 'version', 'insecure_version_date':
                wp_outofdate[value] = self.output[value]
            vulns['wp_outofdate'] = wp_outofdate

        return vulns

    def process(self, vulns):
        """ Process each vuln ID and provide helper tables, etc, to make processing the data easier.
        """

        # Table for wordpress versions
        if 'wp_outofdate' in vulns:
            # Generate a table with [url, version]
            table = []
            for site in vulns['wp_outofdate']['data']:
                table.append([site['url'], site['version']])
            vulns['wp_outofdate']['versions'] = table

        return vulns
