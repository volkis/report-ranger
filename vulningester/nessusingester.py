from ..report_ranger.mdread import markdown_from_file
from ..report_ranger.config import config
import os
import sys
import argparse
import csv
import yaml

parser = argparse.ArgumentParser(
    description="Vuln Ingester gets vuln writeups from the repository, adds info from a Nessus CSV file, and writes it all to a directory for Report Ranger to use.")
parser.add_argument('-i', '--nessuscsv',
                    type=argparse.FileType('r'), help='The Nessus CSV file')
parser.add_argument('-m', '--nessusmapping', type=argparse.FileType('r'),
                    default=open(config['nessusmapper'], 'r'), help='The Nessus mapping YAML file')
parser.add_argument('-o', '--outputdir', type=str,
                    help='The directory to output the files to.')
parser.add_argument('-v', '--verbose', action='store_true', default=config['verbose'],
                    help=f'Turn on verbose mode. Default: {config["verbose"]}')

args = parser.parse_args()

# Turn on verbose mode
vprint = print if args.verbose else lambda *a, **k: None


def ingest_vulns(vulnmapping):
    # We need the vuln repo dir so we can get the vulnerability files
    vulnrepodir = os.path.dirname(os.path.join(
        os.path.curdir, vulnmapping.name))


# Get the nessus mapping
nessusmapping = yaml.safe_load(args.nessusmapping)
vprint(nessusmapping)

# Open the CSV file
nessuscsv = csv.DictReader(args.nessuscsv)

outputdir = args.outputdir

# Make sure the output dir is a directory
if not os.path.isdir(outputdir):
    raise argparse.ArgumentTypeError(
        "Output directory {0} is not a valid directory".format(outputdir))

# Empty dict to hold all the vulnerabilities grouped into Plugin ID
nessusvulns = dict()

# Go through the CSV, group all the vulns by Plugin ID so we can match to writeups
for vuln in nessuscsv:
    # If there's no risk don't bother
    if vuln['Risk'] == "None":
        continue

    pluginid = int(vuln['Plugin ID'])

    if pluginid in nessusmapping:
        # We have a writeup for this vuln
        vprint(
            "+ Adding plugin {}: {}".format(vuln['Plugin ID'], vuln['Name']))

        filename = nessusmapping[pluginid]['file']

        # If we don't have a plugin ID then make one as an empty list
        if filename not in nessusvulns:
            nessusvulns[filename] = []

        # Add it to the list
        nessusvulns[filename] += [vuln]
    else:
        # We don't have a writeup
        vprint(
            "- We don't have a writeup for plugin {}: {}".format(vuln['Plugin ID'], vuln['Name']))


# Get the vuln writeups
for filename in nessusvulns:
    headers, markdown = markdown_from_file(os.path.join(vulnrepodir, filename))

    for host in nessusvulns[filename]:
        pluginid = int(host['Plugin ID'])
        headername = "nessus_" + host['Plugin ID']

        if headername not in headers:
            headers[headername] = []

        newvuln = dict()
        # We have to filter out what we don't want
        if not ('include_host' in nessusmapping[pluginid] and nessusmapping[pluginid]['include_host'] == False):
            # Include the host
            newvuln['host'] = host['Host']

        if not ('include_protocol' in nessusmapping[pluginid] and nessusmapping[pluginid]['include_protocol'] == False):
            # Include the protocol
            newvuln['protocol'] = host['Protocol']

        if not ('include_port' in nessusmapping[pluginid] and nessusmapping[pluginid]['include_port'] == False):
            # Include the host
            newvuln['port'] = host['Port']

        if 'include_plugin_output' in nessusmapping[pluginid] and nessusmapping[pluginid]['include_plugin_output'] == True:
            # Include the host
            newvuln['plugin output'] = host['Plugin Output']

        # Add it to the list
        headers[headername] += [newvuln]

    vprint(headers)

    # Output the YAML code
    finalpath = os.path.join(outputdir, "nr-" + os.path.basename(filename))
    with open(finalpath, 'w') as vulnfile:
        vulnfile.write('---\n')
        vulnfile.write(yaml.dump(headers))
        vulnfile.write('...\n')
        vulnfile.write(markdown)
