---
name: HSTS not set
likelihood: Likely
impact: Critical
suggest_rc:
- "Developer education"
- "System configuration guidelines"
suggest_ar:
- "Developer workshop"
- "System configuration guidelines"
...

### Risk assessment {-}

HSTS is definitely a world-ending vulnerability. We are definitely not hyping up this vulnerability to try and justify our own existance. Penetration testing companies wouldn't do that would they?

### Description {-}

The HSTS policy mitigates the risk of man-in-the-middle attacks by signalling to browsers that different certificates should not be accepted.

### Recommendations {-}

Turn on HSTS. Or not, I'm not your mother.