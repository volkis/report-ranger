---
special: This is a special variable just for this file!
---
This file has been included into the report body.

Special variable in included file. Should display: "This is a special variable just for this file!"

* {{special}}

Testing raw include. Should display "special" with curly brackets around it:

{% raw %}
{{special}}
{% endraw %}
