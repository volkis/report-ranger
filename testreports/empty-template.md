---
---

{{reportbody}}

{{ new_page() }}

{% for section in vulnerabilities.sections %}

{{ new_section() }}

{% if section.name %}

# Detailed Vulnerabilities: {{ section.name }}

{% else %}

# Detailed Vulnerabilities

{% endif %}

## List of vulnerabilities

{{section.list_of_vulnerabilities(of)}}

{{ section.introduction }}

{% for vulnerability in section.vulnerabilities %}

## Vulnerability {{vulnerability.ref}}: {{vulnerability.name}}

{{of.table([['Likelihood', 'Impact', 'Risk'], [vulnerability.headers['likelihood'], vulnerability.headers['impact'], vulnerability.risk]],
headings=[['h', 'h', 'h'], ['', '', 't' + vulnerability.risk.lower().replace(' ', '')]], colalign=['c','c','c'])}}

{{vulnerability.markdown}}

{{new_page() }}

{% endfor %}

{% endfor %}



{% if len(appendices) > 0 %}

{{ new_section() }}
# Appendices {-}

{% for appendix in appendices %}

## Appendix {{appendix.ref}}: {{appendix.name}} {-}

{{appendix.markdown()}}

{{new_page()}}

{% endfor %}

{% endif %}

{{end_report()}}