{
  description =
    "A Nix-flake-based Python development environment for report-ranger";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:

    flake-utils.lib.eachDefaultSystem (system:
      let
        overlays = [ (self: super: { python = super.python310; }) ];

        pkgs = import nixpkgs { inherit overlays system; };
      in {
        devShells.default = pkgs.mkShell {
          LD_LIBRARY_PATH = "${pkgs.stdenv.cc.cc.lib}/lib";
          packages = with pkgs;
            [ python poetry virtualenv ]
            ++ (with pkgs.python310Packages; [ pip ]);

          shellHook = ''
            virtualenv venv
            source venv/bin/activate
            poetry install
          '';
        };
      });
}
