# Builder Image
FROM python:3.10-slim as builder-base

RUN apt-get update && apt-get install --no-install-recommends -y curl build-essential

RUN pip install poetry

WORKDIR /app
COPY poetry.lock pyproject.toml report_ranger ./

RUN poetry build

FROM ubuntu:23.04
WORKDIR /app

#Avoid getting prompted for tz during install
ENV TZ=Australia/Sydney
RUN ln -sf /usr/share/zoneinfo/$TZ /etc/localtime

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    texlive \
    texlive-latex-extra \
    texlive-fonts-extra \
    pandoc \
    lmodern \
    python3 \
    python3-pip \
    && rm -rf /var/lib/apt/lists/*

RUN mkdir /template
RUN mkdir /report

COPY --from=builder-base /app/dist/*.whl .
RUN pip install ./*.whl --break-system-packages
COPY . .

CMD ["python3", "reportranger.py"]
